#!/usr/bin/env python
# cSpell:includeRegExp /".*",/
# cSpell:includeRegExp /\(f?".*"/
#
# Quick way to create & delete Proxmox VE LXC for YugabyteDB lab
# Another way is to use the community.general.proxmox module
#
# You must be logged in as root on one of the ProxmoxVE hosts
# You can easily adapt the script in case you have only one ProxmoxVE server

import secrets
import string
import os
from ipaddress import ip_address

first_ip = ip_address('192.168.10.81')
default_route = ip_address('192.168.10.1')
first_vmid: int = 211           # Make sure the vmids from 211 to 213 are free in PVE or change
ssh_pubkey: str = os.path.expanduser('~/.ssh/id_rsa.pub')
memory_MB: int = 1024*8         # 8GB Ram
storage: str = 'localblock'
storage_GB: int = 8
cores: int = 2
# The OS template must be available from each PVE host (here from a shared filesystem)
pve_os_template: str = \
    "/mnt/pve/cephfs/template/cache/ubuntu-22.04-standard_22.04-1_amd64.tar.zst"
lxcpool: str = "YugabyteDB"


with open(ssh_pubkey) as f:
    ssh_key: str = f.read().strip()


print('1. Create LXCs')
print('==============')
print(f'pvesh create /pools --poolid { lxcpool }')
for hid in range(3):
    pw: str = ''.join(
        [secrets.choice(string.ascii_letters) for _ in range(10)]
    )

    ip: str = f"{ first_ip + hid }/24"

    print(
        (
            f'pvesh create /nodes/pve{ hid + 1 }/lxc -vmid '
            f'{ first_vmid + hid } '
            f'-ostemplate={ pve_os_template } '
            f'-hostname=yugabytedb{ hid + 1 } -memory={ memory_MB } -swap=512 -cores={cores}'
            f'-net0="name=eth0,bridge=vmbr0,firewall=0,gw={ default_route },ip={ ip },tag=10,type=veth" '
            f'-storage={ storage } -rootfs={ storage }:{ storage_GB } -unprivileged=1 '
            f'-pool={ lxcpool } -ignore-unpack-errors '
            f'-ssh-public-keys="{ ssh_key }" '
            f'-ostype=ubuntu -start=1 '
            # f'-password={pw} ' # Uncomment this line if you want to set a root password
        )
    )

print('Shutdown LXCs & Delete LCSs')
print('===========================')
for hid in range(3):
    print(f'pvesh create /nodes/pve{ hid + 1 }/lxc/{ first_vmid + hid }/status/shutdown')

for hid in range(3):
    print(f'pvesh delete /nodes/pve{ hid + 1 }/lxc/{ first_vmid + hid }')
print('pvesh delete /pools/YugabyteDB')
