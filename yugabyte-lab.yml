---
# cSpell:includeRegExp /".*",/
- hosts: yugabytedb
  tasks:
    - name: Install auto-apt-proxy
      ansible.builtin.apt:
        pkg:
          - auto-apt-proxy

    - name: Update cache
      ansible.builtin.apt:
        update_cache: true
        cache_valid_time: 86400

    - name: Upgrade packages
      ansible.builtin.apt:
        upgrade: true

    - name: Install required packages
      ansible.builtin.apt:
        pkg:
          - neovim
          - python-is-python3
          - xfsprogs
          - curl

    - name: Check if Yugabyte folder exists
      ansible.builtin.stat:
        path: "{{ yugabytedb_folder }}"
      register: ybpath

    - name: Create the Yugabyte DB folder
      ansible.builtin.file:
        path: "{{ yugabytedb_folder }}"
        state: directory
      when: not ybpath.stat.exists

    - name: Download and extract YugabyteDB software
      ansible.builtin.unarchive:
        src: "{{ yugabytedb_url }}"
        dest: "{{ yugabytedb_folder }}"
        remote_src: true
        extra_opts: [--strip-components=1]
      when: not ybpath.stat.exists

    - name: YugabyteDB Post Install script
      ansible.builtin.shell:
        cmd: "{{ yugabytedb_folder }}/bin/post_install.sh"
        chdir: "{{ yugabytedb_folder }}"

    - name: Create the YugabyteDB data folder
      ansible.builtin.file:
        path: "{{ yugabytedata_folder }}"
        state: directory

    - name: Create the yb-master systemd files
      tags: ["systemd-files"]
      ansible.builtin.template:
        src: templates/yb-master.service.j2
        dest: /etc/systemd/system/yb-master.service

    - name: Create the yb-tserver systemd files
      tags: ["systemd-files"]
      ansible.builtin.template:
        src: templates/yb-tserver.service.j2
        dest: /etc/systemd/system/yb-tserver.service

- name: Start the YB-Master
  hosts: yugabytedb
  serial: 1
  tasks:
    - name: Restart yb-master service
      tags: ["systemd-files"]
      ansible.builtin.systemd:
        name: yb-master
        state: restarted
        enabled: yes
        daemon_reload: yes

    - name: Pause 10s
      ansible.builtin.pause:
        seconds: 10

- name: Start the YB-TServers
  hosts: yugabytedb
  serial: 1
  tasks:
    - name: Restart yb-tserver service
      tags: ["systemd-files"]
      ansible.builtin.systemd:
        name: yb-tserver
        state: restarted
        enabled: yes
        daemon_reload: yes

    - name: Pause 10s
      ansible.builtin.pause:
        seconds: 10

    - name: Set the replica placement policy
      tags: ["placement_policy"]
      ansible.builtin.command:
        cmd: >-
          {{ yugabytedb_folder }}/bin/yb-admin --master_addresses 
          {{ groups['yugabytedb'] | map('extract', hostvars, ['ansible_ssh_host']) | join(':7100,') }}:7100 
          modify_placement_info 
          {{ groups['yugabytedb'] | map('extract', hostvars, ['placement_zone']) | map('regex_replace', '^(.*)$', placement_cloud + '.' + placement_region + '.' + '\1') |join(',') }} 3
        chdir: "{{ yugabytedb_folder }}"
      when: ansible_hostname[-1] == '3'
